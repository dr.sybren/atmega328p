#include "usart.h"
#include "hardware.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#include <string.h>

// The queue is on interval [tx_buf_tx_head, tx_buf_queue_head) mod TX_BUF_SIZE.
#define TX_BUF_SIZE 64
uint8_t tx_buffer[TX_BUF_SIZE];
uint8_t *tx_buf_tx_head = tx_buffer;
uint8_t *tx_buf_queue_head = tx_buffer;

void _synchronous_send_byte(const uint8_t data);
bool _send_queued_byte(void);

void usart_setup(void)
{
  // Configure UART0
  UBRR0 = 25;           // baud rate 38400
  UCSR0A = 0            //
           | 0 << U2X0  // Double baud rate
      ;
  UCSR0B = 0              //
           | 1 << RXEN0   // Receiver enable
           | 1 << TXEN0   // Transmitter enable
           | 1 << RXCIE0  // Receiver interrupt
      ;
  UCSR0C = 0                  //
           | 0b00 << UMSEL00  // Mode select
           | 0b00 << UPM00    // Party: None
           | 0b0 << USBS0     // Stop bits: 1
           | 0b11 << UCSZ00   // Data bits: 8
           | 0b1 << UCPOL0    // Clock polarity
      ;
}

int8_t usart_tx_buf_space(void)
{
  if (tx_buf_tx_head <= tx_buf_queue_head) {
    // Not wrapped.
    return TX_BUF_SIZE - (tx_buf_queue_head - tx_buf_tx_head) - 1;
  }
  // Wrapped.
  return tx_buf_tx_head - tx_buf_queue_head - 1;
}

void usart_tx_queue_byte(const uint8_t byte)
{
  while (usart_tx_buf_space() < 1)
    __asm__ __volatile__("nop");

  // Push byte onto queue
  *tx_buf_queue_head = byte;
  ++tx_buf_queue_head;
  if (tx_buf_queue_head - tx_buffer == TX_BUF_SIZE) {
    tx_buf_queue_head = tx_buffer;
  }

  // Enable the interrupt to actually send the byte.
  UCSR0B |= (1 << UDRIE0);
  PORTB |= 1 << PORTB5;  // Turn on debug LED
}

void usart_tx_queue_string(const char *string)
{
  while (*string) {
    usart_tx_queue_byte(*string);
    ++string;
  }
}

void usart_tx_queue_float(const float value)
{
  /* Build up a buffer in reverse order. */
  char buffer[10];
  char *buf_front = buffer;
  uint16_t decimals;  // variable to store the decimals
  int units;          // variable to store the units (part to left of decimal place)
  if (value < 0) {    // take care of negative numbers
    decimals = (int)(value * -100) % 100;  // make 1000 for 3 decimals etc.
    units = (int)(-1 * value);
  }
  else {  // positive numbers
    decimals = (int)(value * 100) % 100;
    units = (int)value;
  }

  *++buf_front = (decimals % 10) + '0';
  decimals /= 10;  // repeat for as many decimal places as you need
  *++buf_front = (decimals % 10) + '0';
  *++buf_front = '.';

  while (units > 0) {
    *++buf_front = (units % 10) + '0';
    units /= 10;
  }
  if (value < 0)
    *++buf_front = '-';  // unary minus sign for negative numbers

  for (; buf_front > buffer; --buf_front) {
    usart_tx_queue_byte(*buf_front);
  }
}

ISR(USART_UDRE_vect)
{
  if (!_send_queued_byte()) {
    // Transmission done, disable the interrupt.
    UCSR0B &= ~(1 << UDRIE0);
    PORTB &= ~(1 << PORTB5);  // Turn off debug LED
  }
}

bool _send_queued_byte(void)
{
  if (tx_buf_tx_head == tx_buf_queue_head)
    // Queue is empty.
    return false;

  // _synchronous_send_byte(*tx_buf_tx_head);
  UDR0 = *tx_buf_tx_head;
  tx_buf_tx_head++;

  if (tx_buf_tx_head - tx_buffer == TX_BUF_SIZE) {
    tx_buf_tx_head = tx_buffer;
  }

  return true;
}

void _synchronous_send_byte(const uint8_t data)
{
  PORTB |= 1 << PORTB5;  // Turn on debug LED

  /* Wait for empty transmit buffer */
  while (!(UCSR0A & (1 << UDRE0)))
    ;
  /* Put data into buffer, sends the data */
  UDR0 = data;

  _delay_ms(50);
  PORTB &= ~(1 << PORTB5);  // Turn off debug LED
}

ISR(USART_RX_vect)
{
  /* Simple echo for now. */
  const uint8_t rx_byte = UDR0;

  sei();

  usart_tx_queue_byte(rx_byte);
  if (rx_byte == '\r')
    usart_tx_queue_byte('\n');
}
