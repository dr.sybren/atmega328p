#pragma once

#include <stdbool.h>
#include <stdint.h>

void usart_setup(void);
int8_t usart_tx_buf_space(void);

void usart_tx_queue_byte(uint8_t byte);
void usart_tx_queue_string(const char *string);
void usart_tx_queue_float(float value);
