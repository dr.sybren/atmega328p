
#include "hardware.h"
#include "thermometer.h"
#include "usart.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/delay.h>

void setup(void);
void loop(void);
void spinner(void);

int main(void)
{
  setup();

  usart_tx_queue_string("\033[2J");    // Clear screen
  usart_tx_queue_string("\033[?25l");  // Hide cursor
  usart_tx_queue_string("\r---------------------\r\n");
  usart_tx_queue_string("\033[95mSybren\033[0m's \033[92mUSB Chippie!\033[0m\r\n");
  usart_tx_queue_string("---------------------\r\n");
  usart_tx_queue_string("\r\n");

  for (;;)
    loop();
  return 0;
}

void loop()
{
  const float temp = thermometer_read_temp();

  usart_tx_queue_string("Temperature: ");
  usart_tx_queue_float(temp);
  usart_tx_queue_string("\r\n");

  _delay_ms(1000);
}

void setup()
{
  // Always disable watchdog reset at startup.
  MCUSR &= ~(1 << WDRF);
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0;

  PRR = 0                //
        | (1 << PRTIM0)  // Timer 0
        | (1 << PRTIM1)  // Timer 1
        | (1 << PRTIM2)  // Timer 2
        | (1 << PRADC)   // ADC
      ;

  usart_setup();

  // PB5 is built-in LED output, PB4 is external LED.
  DDRB = 1 << DDB4 | 1 << DDB5;
  PORTB = 0;

  sei();

  thermometer_setup();
}

void spinner(void)
{
  static const char spinner[] = "-\\|/";
  static int spinner_idx = 0;

  spinner_idx = (spinner_idx + 1) % 4;
  usart_tx_queue_byte(spinner[spinner_idx]);
}
