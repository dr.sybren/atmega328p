#include <Arduino.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "Adafruit_SSD1306.h"

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

char display_string_buffer[80];
char float_format_buffer[80];
volatile int rawtemp = 327;

const uint8_t WAKEUP_PIN = 2;
volatile bool woken_up = false;
unsigned long wakeup_millis = 0;
unsigned long wakeup_watchdog_count = 0;

void display_setup()
{
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    delay(250);

    void (*reset)() = 0;
    reset();
  }

  display.dim(false);
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.display();
}


void watchdog_setup()
{
  MCUSR &= ~(1<<WDRF);
  WDTCSR = 1<<WDCE | 1<<WDE;
  WDTCSR = 1<<WDIE | 0b0111<<WDP0;

  // Disable unused interrupts so we can sleep.
  PCMSK0 = 0;
  PCMSK1 = 0;
  PCMSK2 = 0;
  PCICR = 0;
}

void int_wakeup()
{
  woken_up = true;
  wakeup_millis = millis();
  wakeup_watchdog_count = 0;
}

void setup()
{
  // Serial.begin(115200);
  // Serial.println("----------------------");
  display_setup();
  watchdog_setup();

  attachInterrupt(digitalPinToInterrupt(WAKEUP_PIN), int_wakeup, FALLING);
  pinMode(WAKEUP_PIN, INPUT_PULLUP);

  ADMUX = 0b11<<REFS0 | 0b1000<<MUX0;
  ADCSRA = 1<<ADEN | 0b11<<ADPS0;
  ADCSRB = 0;
}

void readADC()
{
  ADMUX = 0b11<<REFS0 | 0b1000<<MUX0;
  ADCSRA |= 1 << ADSC | 1 << ADIE;

  set_sleep_mode(SLEEP_MODE_ADC);
  sleep_mode();
}

// ISR just needs to exist for the interrupt to wake us up.
ISR(WDT_vect) {}

ISR(ADC_vect)
{
  rawtemp = ADC;
}

void loop()
{
  readADC();

  static const float ad2volts = 1.1/1023.0;
  static const float volts2degC = 25.0 / 0.314; 
  static float smooth_temp = -400;
  float temp = rawtemp * ad2volts * volts2degC;

  // Have a few measurements before we use the result.
  if (smooth_temp < -300) {
    smooth_temp += 10;
    return;
  }
  if (smooth_temp < -280) {
    smooth_temp = temp;
  }
  smooth_temp += 0.1 * (temp - smooth_temp);

  display.clearDisplay();
  if (woken_up) {
    display.setCursor(0,0);

    dtostrf(smooth_temp, 6, 3, float_format_buffer);
    sprintf(display_string_buffer, "%s C", float_format_buffer);
    display.setTextSize(2);
    display.print(display_string_buffer);

    display.setTextSize(1);
    display.setCursor(display.getCursorX()-18, -2);
    display.print("o");
  }
  display.display();

  if (millis() < 10000) {
    delay(25);
    if (woken_up && millis() - wakeup_millis > 5000) {
      woken_up = false;
    }
  }
  else {
    if (woken_up && wakeup_watchdog_count++ > 2) {
      woken_up = false;
    }
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_mode();
  }
}
