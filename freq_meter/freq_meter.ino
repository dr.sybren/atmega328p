#include <Arduino.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>

volatile uint8_t timer1_overflows = 0;
volatile uint8_t timer2_overflows = 0;
unsigned int sampling_period = 100;  // in milliseconds

// Factor by which to multiply to get to a 16 MHz clock.
// So > 1 means the clock is slower than 16 MHz.
#define CLOCK_CALIBRATE 1.04999054999

void setup()
{
  Serial.begin(115200);
  Serial.println("je moeder");

  DDRB = 0;
  PORTB = 0;

  DDRD = 1 << 5;
  PORTD = 0;

  // Timer 0
  TCCR0A = 0
    | 0b10 << COM0B0
    | 0b01 << WGM00
  ;
  TCCR0B = 0
    | 1 << WGM02
    | 0b011 << CS00
  ;
  TIMSK0 = 0;
  OCR0A = 32;
  OCR0B = OCR0A >> 1;

  // Timer 1
  TCCR1A = 0;
  TCCR1B = 0
    | 0b111 << CS10
  ;
  TIMSK1 = 0
    | 1 << TOIE1
  ;

  // Timer 2
  TCCR2A = 0b10 << WGM20; // CTC mode
  TCCR2B = 0b101 << CS20; // prescaler set to 1/128, "ticks" at 125 kHz
  OCR2A = 124; // counts from 0 to 124, then fire interrupt and reset;

  TCNT1 = TCNT2 = 0; // set current timer value to 0
  TIMSK2 = 1 << OCIE2A;

  // ADC
  ADCSRB = 0
    // | 0b000 << ADTS0
  ;
  ADCSRA = 0
    | 1 << ADEN      // enable ADC
    | 0b111 << ADPS0 // clock divider
    | 1 << ADATE     // auto-trigger
    | 1 << ADSC      // start auto-run
  ;
  ADMUX = 0
    | 0b001 << REFS0  // Vcc as Vref
    | 0b0011 << MUX0  // read from PC3  
    // | 1 << ADLAR
  ;
}

ISR(TIMER1_OVF_vect)
{
    timer1_overflows++;
}

// interrupt happens at each 125 counts / 125 kHz = 0.001 seconds = 1 ms
ISR(TIMER2_COMPA_vect)
{
  if (++timer2_overflows < sampling_period) {
    return; // still sampling
  }

  uint32_t total_samples = (((uint32_t)timer1_overflows) << 16) + TCNT1;  
  float freqHz = total_samples * 1000.0 / sampling_period * CLOCK_CALIBRATE;

  // Timer 0 uses clkI/O without prescaler, so at 8 MHz
  // (phase correct PWM, is at half the clock speed).
  float compHz = 8000000. / 64. / OCR0A * CLOCK_CALIBRATE;

  Serial.print("ADC: ");
  Serial.print(ADC);
  Serial.print("  OCR0A: ");
  Serial.print(OCR0A);
  Serial.print("  Computed: ");
  Serial.print((uint32_t)compHz);
  Serial.print(" Hz");
  Serial.print("  Measured: ");
  Serial.print((uint32_t)freqHz);
  Serial.println(" Hz");

  // reset timers
  ATOMIC_BLOCK(ATOMIC_FORCEON) {
    GTCCR |= 1 << TSM;
    TCNT1 = 0; timer1_overflows = 0;
    TCNT2 = 0; timer2_overflows = 0;
    GTCCR &= ~(1 << TSM);
  }
}


void loop()
{
#define MIN_OCR0A 60.
#define MAX_OCR0A 68.
#define MAX_ADC 930.
#define ADC_FACTOR ((MAX_OCR0A - MIN_OCR0A) / MAX_ADC)

  // OCR0A = (CLOCK_CALIBRATE * 8000000. / 64.) / 2048.;
  // OCR0B = OCR0A >> 1;

  double target_ocr0a = MAX_OCR0A - ADC_FACTOR * ADC;
  target_ocr0a = max(0., min(target_ocr0a, 255.));
  
  static double smooth_ocr0a = 0;
  double error = target_ocr0a - smooth_ocr0a;
  if (fabs(error) < 0.01) {
    smooth_ocr0a = target_ocr0a;
  }
  else {
    smooth_ocr0a += error * 0.01;
  }
  ATOMIC_BLOCK(ATOMIC_FORCEON) {
    OCR0A = smooth_ocr0a;
    OCR0B = OCR0A >> 1;
  }

  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();
}
