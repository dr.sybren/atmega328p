#include <avr/interrupt.h>

#include "lcd_i2c_driver.h"

int main()
{
    sei();

    lcd_i2c_begin(0x27, 16, 2, LCD_5x8DOTS);
    lcd_i2c_text("134134143");

    for (;;)
        ;
}
