/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once

#include <stdint.h>

#include "pair.hh"

class CircularBuffer {
  // The queue is on interval [buf_data_head, buf_queue_head) mod buf_size_.
  static constexpr uint8_t buf_size_ = 64;
  uint8_t buffer_[buf_size_];
  uint8_t *buf_data_head_ = buffer_;
  uint8_t *buf_queue_head_ = buffer_;

 public:
  // Space available in the buffer.
  uint8_t space() const;
  bool is_empty() const;

  void queue_byte(uint8_t byte);
  pair<uint8_t, bool> pop_byte();
};
