/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include <stdbool.h>
#include <stdint.h>

#include "i2cmaster.h"
#include "mcp4725_dac_driver.h"

const uint8_t MCP4725_ADDRESS_A0_LOW = 0b01100000;        // 0x60
const uint8_t MCP4725_ADDRESS_GENERAL_CALL = 0b00000000;  // 0x00
const uint8_t MCP4725_GENERAL_CALL_RESET = 0b00000110;    // 0x06
const uint8_t MCP4725_WRITE_DAC = 0b01000000;             // 0x40

static uint8_t mcp4725_i2c_addr = MCP4725_ADDRESS_A0_LOW;

static void write_dac(bool write_eeprom, uint16_t dac_value);

void mcp4725_init(const uint8_t i2c_addr)
{
  mcp4725_i2c_addr = i2c_addr;
}

void mcp4725_general_call_reset()
{
  i2c_start_wait(MCP4725_ADDRESS_GENERAL_CALL, I2C_WRITE);
  i2c_write(MCP4725_GENERAL_CALL_RESET);
  i2c_stop();
}

void mcp4725_write_dac(const uint16_t dac_value)
{
  write_dac(false, dac_value);
}
void mcp4725_write_dac_and_eeprom(const uint16_t dac_value)
{
  write_dac(true, dac_value);
}

static void write_dac(const bool write_eeprom, const uint16_t dac_value)
{
  i2c_start_wait(mcp4725_i2c_addr, I2C_WRITE);
  // TODO: include power-down mode in the config.
  i2c_write(MCP4725_WRITE_DAC | (write_eeprom << 5));
  i2c_write(dac_value >> 4);
  i2c_write((dac_value & 0xF) << 4);
  i2c_stop();
}

MCP4725_DAC_Status mcp4725_read_status()
{
  i2c_start_wait(mcp4725_i2c_addr, I2C_READ);  // This is byte1.
  const uint8_t byte2 = i2c_read(true);
  const uint8_t byte3 = i2c_read(true);
  const uint8_t byte4 = i2c_read(true);
  const uint8_t byte5 = i2c_read(true);
  const uint8_t byte6 = i2c_read(false);
  i2c_stop();

  MCP4725_DAC_Status status;
  status.is_ready = (byte2 & 0b10000000) != 0;
  status.power_status = (byte2 >> 1) & 0b11;
  status.is_power_down = status.power_status != 0;

  status.dac_value = (byte3 << 4) | (byte4 >> 4);
  status.eeprom_power_status = (byte5 >> 5) & 0b11;
  status.eeprom_value = ((byte5 & 0b1111) << 8) | byte6;
  return status;
}

void mcp4725_stream_start()
{
  i2c_start_wait(mcp4725_i2c_addr, I2C_WRITE);
}
void mcp4725_stream_value(const uint16_t dac_value)
{
  i2c_write(MCP4725_WRITE_DAC);
  i2c_write(dac_value >> 4);
  i2c_write((dac_value & 0xF) << 4);
}
void mcp4725_stream_stop()
{
  i2c_stop();
}

void mcp4725_stream_value_dampened(const uint16_t dac_value, const float dampening_factor)
{
  int16_t signed_value = dac_value;  // This'll fit as the DAC value is only 12 bit.
  mcp4725_stream_value((signed_value - 2048) * dampening_factor + 2048);
}
