/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "hardware.h"
#include "interrupts.h"
#include "mcp4725_dac_driver.h"
#include "setup.h"
#include "sine.h"
#include "usart.h"

#include <math.h>
#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

static void usb_show_intro(void);
static void usb_terminal_refresh(void);

static void report_reset_cause();
static void report_state();
static void periodic_report_state();

static void handle_usart_rx_byte(uint8_t rx_byte);

static void do_dac_value(uint16_t value);

volatile bool sending_sine;
volatile uint16_t sine_phase;

int main()
{
  setup();
  sei();

  report_reset_cause();

  mcp4725_general_call_reset();
  usart << "DAC reset\r\n";

  bool streaming = false;
  float volume = 1.0f;

  for (;;) {
    if (sending_sine) {
      if (!streaming) {
        usart << "Starting streaming\r\n";
        mcp4725_stream_start();
        streaming = true;
        sine_phase = 0;
        volume = 1.0f;
        time_centiseconds = 0;
      }

      const int16_t sine_value = sine(sine_phase);
      mcp4725_stream_value_dampened(sine_value, volume);

      sine_phase++;
      if (sine_phase >= sine_table_size()) {
        sine_phase = 0;
      }

      volume *= 0.999f;
      if (volume < 0.01f) {
        sending_sine = false;
      }
    }
    else if (streaming) {
      mcp4725_stream_value(2048);
      mcp4725_stream_stop();
      usart << "Stopping streaming after " << (uint16_t)(time_centiseconds * 10) << " msec\r\n";
      streaming = false;
    }

    while (usart_rx_buf_has_data()) {
      const uint8_t rx_byte = usart_rx_buf_pop_byte();
      handle_usart_rx_byte(rx_byte);
    }

    wdt_reset();

    if (!sending_sine) {
      set_sleep_mode(SLEEP_MODE_IDLE);
      sleep_mode();
    }
  }
}

static void report_power_state(const MCP4725_Power_Status power_status)
{
  switch (power_status) {
    case MCP4725_POWER_UP:
      usart << "awake";
      break;
    case MCP4725_POWER_DOWN_1K:
      usart << "down; 1K pull-down";
      break;
    case MCP4725_POWER_DOWN_100k:
      usart << "down; 100k pull-down";
      break;
    case MCP4725_POWER_DOWN_500k:
      usart << "down; 500k pull-down";
      break;
    default:
      usart << "unknown value " << power_status;
      break;
  }
};

static void report_state()
{
  if (sending_sine) {
    usart << "sending sine wave\r\n";
  }
  else {
    usart << "not sending sine wave\r\n";
  }
  usart << "polling DAC\r\n";
  const MCP4725_DAC_Status status = mcp4725_read_status();
  usart << "DAC status: " << (status.is_ready ? "ready" : "not ready") << "\r\n";
  usart << "     power: ";
  report_power_state(status.power_status);
  usart << " (boot ";
  report_power_state(status.eeprom_power_status);
  usart << ")\r\n";
  usart << "     value: " << status.dac_value << " (boot " << status.eeprom_value << ")\r\n";
}

static void periodic_report_state()
{
  static uint8_t last_time_mod100 = 0;
  const uint8_t time_mod100 = time_centiseconds % 100;

  if (time_mod100 == last_time_mod100) {
    return;
  }
  last_time_mod100 = time_mod100;

  if (time_mod100 == 0) {
    report_state();
  }
}

static void usb_terminal_refresh()
{
  usb_show_intro();
}

static void handle_usart_rx_byte(uint8_t rx_byte)
{
  switch (rx_byte) {
    case 's':  // Status report.
      report_state();
      break;
    case 'r':  // Reboot.
      usart << "Sending general call DAC reset\r\n";
      mcp4725_general_call_reset();
      break;
    case '`':
      do_dac_value(0);
      break;
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      do_dac_value((rx_byte - '0') * 409);
      break;
    case '0':
      do_dac_value(4095);
      break;
    case 'l':  // Lock up.
      usart << "Locking up\r\n";
      for (;;)
        ;
      break;
    case '/':
    case 'L' - 'A' + 1:  // Ctrl+L
      usb_terminal_refresh();
      break;
    case 'w':  // Send sine wave
      if (sending_sine) {
        usart << "W: aborting sine wave\r\n";
        sine_phase = 0;
        sending_sine = false;
        break;
      }

      usart << "W: initiating sine wave\r\n";
      sine_phase = 0;
      sending_sine = true;
      break;
  }
}

static void usb_show_intro(void)
{
  // Source: https://www2.ccs.neu.edu/research/gpc/VonaUtils/vona/terminal/vtansi.htm
  usart << "\033[7l";    // Disable line wrap
  usart << "\033[r";     // Set scrolling region to entire screen.
  usart << "\033[2J";    // Clear screen
  usart << "\033[?25l";  // Hide cursor
  usart << "\r---------------------------\r\n";
  usart << " \033[95mSybren\033[0m's \033[92mDAC test\033[0m\r\n";
  usart << "---------------------------\r\n";
  usart << "r: \033[1mr\033[0meboot device\r\n";
  usart << "s: \033[1ms\033[0mtatus report\r\n";
  usart << "l: \033[1ml\033[0mock up the device\r\n";
  usart << "/: reset terminal\r\n";
  usart << "---------------------------\r\n";
  usart << "\r\n";

  constexpr uint16_t scroll_end = 50;
  usart << "\033[" << (scroll_end + 1) << ";0H";  // Move cursor
  usart << "---------------------------\r\n";

  usart << "\033[10;" << scroll_end << "r";  // Set scrolling region
  usart << "\033[10;0H";                     // Move cursor
}

static void report_reset_cause()
{
  usart << "Device booted from ";
  if (MCUSR & (1 << WDRF)) {
    usart << "watchdog";
  }
  else if (MCUSR & (1 << BORF)) {
    usart << "brown-out";
  }
  else if (MCUSR & (1 << EXTRF)) {
    usart << "external";
  }
  else if (MCUSR & (1 << PORF)) {
    usart << "power-on";
  }
  else {
    usart << "unknown";
  }
  usart << " reset\r\n";
  MCUSR &= ~((1 << WDRF) | (1 << BORF) | (1 << EXTRF) | (1 << PORF));
}

static void do_dac_value(const uint16_t value)
{
  usart << "Setting DAC to " << value << "\r\n";
  mcp4725_write_dac(value);
}
