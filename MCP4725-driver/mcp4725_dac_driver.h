/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

extern const uint8_t MCP4725_ADDRESS_A0_LOW;

typedef enum {
  MCP4725_POWER_UP = 0b00,
  MCP4725_POWER_DOWN_1K = 0b01,
  MCP4725_POWER_DOWN_100k = 0b10,
  MCP4725_POWER_DOWN_500k = 0b11,
} MCP4725_Power_Status;

typedef struct {
  bool is_ready;
  bool is_power_down;
  MCP4725_Power_Status power_status;
  MCP4725_Power_Status eeprom_power_status;
  uint16_t dac_value;
  uint16_t eeprom_value;
} MCP4725_DAC_Status;

void mcp4725_init(uint8_t i2c_addr);
void mcp4725_general_call_reset(void);

void mcp4725_write_dac(uint16_t dac_value);
void mcp4725_write_dac_and_eeprom(uint16_t dac_value);

void mcp4725_stream_start();
void mcp4725_stream_value(uint16_t dac_value);
void mcp4725_stream_value_dampened(uint16_t dac_value, float dampening_factor);
void mcp4725_stream_stop();

MCP4725_DAC_Status mcp4725_read_status(void);

#ifdef __cplusplus
}
#endif
