/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "setup.h"
#include "hardware.h"
#include "i2cmaster.h"
#include "mcp4725_dac_driver.h"
#include "usart.h"

#include <stdbool.h>

#include <avr/io.h>

#define MCP_I2C_ADDR 0x18

static void setup_timers();

void setup()
{
  // Enable watchdog.
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0                 //
           | 1 << WDE        // Watchdog Enable
           | 0b0110 << WDP0  // 1s
      ;

  // Set the expected clockdiv.
  CLKPR = 1 << CLKPCE;
#if F_CPU == 8000000 || F_CPU == 16000000
  CLKPR = 0b0000;
#elif F_CPU == 1000000
  CLKPR = 0b0011;
#else
#  error Unexpected value for F_CPU
#endif

  PCICR = 0;  // Disable PCINT except the ones we configure.
  GTCCR = 0;  // Disable any timer sync at boot.

  DDRB = DDRC = DDRD = 0;        // All pins as input by default.
  PORTB = PORTC = PORTD = 0;     // Disable all pull-ups.
  PCMSK0 = PCMSK1 = PCMSK2 = 0;  // Disable all pin change interrupts.

  // The big LEDs are active low, so minimize the startup-blink caused by the default-low of the
  // output pins.
  PORTB |= (1 << PB1) | (1 << PB2);

  PRR = 0               //
        | (1 << PRSPI)  // SPI bus
      ;

  // AUX LED on PB5.
  DDRB = 1 << DDB5;

  usart_setup();
  i2c_init();
  mcp4725_init(MCP4725_ADDRESS_A0_LOW);
  setup_timers();
}

static void setup_timers()
{
  // Timer 0 for keeping track of time.
#if F_CPU != 16000000
#  error Timer 0 config assumes 16 MHz clock
#endif
  TCCR0A = 0b10 << WGM00;   // WGM = 0b010 = CTC
  TCCR0B = 0                //
           | 0b0 << WGM02   //
           | 0b100 << CS00  // clockdiv
      ;
  OCR0A = 78;  // ~100 Hz interrupts
  TIFR0 = 0;   // Clear all interrupt flags
  TIMSK0 = 1 << OCIE0A;
}
